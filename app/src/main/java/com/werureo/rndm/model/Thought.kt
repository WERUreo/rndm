package com.werureo.rndm.model

import java.util.*

data class Thought constructor(
        val username: String,
        val timestamp: Date,
        val thoughtTxt: String,
        val numLikes: Int,
        val numComments: Int,
        val documentId: String,
        val userId: String
)