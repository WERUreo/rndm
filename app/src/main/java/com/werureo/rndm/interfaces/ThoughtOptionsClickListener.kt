package com.werureo.rndm.interfaces

import com.werureo.rndm.model.Thought


interface ThoughtOptionsClickListener {
    fun thoughtOptionsMenuClicked(thought: Thought)
}