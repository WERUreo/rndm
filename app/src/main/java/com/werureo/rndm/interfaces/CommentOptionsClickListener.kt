package com.werureo.rndm.interfaces

import com.werureo.rndm.model.Comment


interface CommentOptionsClickListener {
    fun commentOptionsMenuClicked(comment: Comment)
}